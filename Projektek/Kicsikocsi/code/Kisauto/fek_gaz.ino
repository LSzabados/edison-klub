#include "includes.h"


// ----------------------------------------------------------------------------------------------------
void fek_gaz()
{
  unsigned char szazalek;
  unsigned short int speed,pwm;
  
  speed = GAZ_ALLAS;
  szazalek = sebesseg2Szazalek(speed);    // konvertalas szazalekra 0-100
  pwm = sebesseg2pwm(speed);              // konvertalas 8 bitre 0-255

  // feklampa es PWM beallitasa
  if (FEK_NYOMVA)   // ha nyomva a fek, nem engedjuk forogni a motort
  {
    pwm = 0;
    FEKLAMPA_BE;
  }
  else
  {
    FEKLAMPA_KI;
  }

  // Irany es sebesseg beallitasa
  if (Sebvalto == Elore)
  {
	  IRANY_ELORE;
    PWM_BEALLITASA(pwm);
    TOLATOLAMPA_KI;
  }
  
  if (Sebvalto == Hatra)
  {
	  IRANY_HATRA;
    PWM_BEALLITASA(pwm);
    TOLATOLAMPA_BE;
  }

  if ((Sebvalto == Park) || (Sebvalto == Ures))
  {
    PWM_BEALLITASA(0);    // motor letiltva
    TOLATOLAMPA_KI;
  }

  // Fek beallitasa
  if ((Sebvalto == Park) || FEK_NYOMVA)
    FEK_ROGZITES;
  else
    FEK_OLDAS;

  // Fek kijelzese
  lcd.setCursor(8,1);
  if (FEK_NYOMVA)
    lcd.print("FEK");
  else
    lcd.print("   ");

  // Gaz kijelzese
  gaz_kijelzes(szazalek);  
  // Biztonsági Park üzemmód
  park_biztonsagi(szazalek);

 // Akku feszultseg kijelzese
  float fesz = AKKU_FESZ_ERTEK;
  lcd.setCursor(0,1);
  lcd.print(fesz);
  lcd.print("V ");
}
 
 
#define PB_TIMEOUT 30000                                         // Ennyi gáz inaktivitás után visszaállunk Parkba
unsigned long int PB_ido;                                        // Időbélyeg a gázpedál figyelésére

void park_biztonsagi(unsigned char szazalek)
{
  unsigned long int eltelt_ido;

  if ((Sebvalto == Park) || (szazalek>0))                       // ha nyomjuk a gázt vagy Parkban vagyunk
    PB_ido=millis();                                            // lementjük az időbélyeget

  eltelt_ido=millis()-PB_ido;                                   // Meghatarozzuk az eltelt idot
                                                                // Az utolso gazlenyomas ota
  if (eltelt_ido>PB_TIMEOUT)                                    // Ha ez nagyobb 
    Sebvalto=Park;                                              // Parkba kapcsol
}


// Gaz kijelzese
void gaz_kijelzes(unsigned char szazalek)
{
  unsigned char  v;
  
  lcd.setCursor(8,2);
  lcd.print(szazalek);
  lcd.print("%  ");
  
  lcd.setCursor(0,3);
  for(v=5;v<=100;v=v+5)
  {
   if (v<=szazalek)
   lcd.write((uint8_t)255);   // tele karakter
   else
   lcd.print(" ") ;
  }
}

