
// Gazpedal poti szelso helyzetek
#define GAZ_ALAPJARAT 350
#define GAZ_FULL      760

 
// Analog bemenetek
#define AKKU_FESZ     0
#define SEBESSEG_POTI 1
#define FEK_KAPCSOLO  2

// Digitalis labak
#define LCD_EN_PIN    21    // Uno 19 - Mega 21
#define LCD_RS_PIN    20    // Uno 18 - Mega 20
#define LCD_D4_PIN    10
#define LCD_D5_PIN    11
#define LCD_D6_PIN    12
#define LCD_D7_PIN    13

// PWM lab 
#define PWM_PIN   	9

// Rele kimenetek
#define RELE1_PIN     0
#define RELE2_PIN     1
#define RELE3_PIN     2
#define RELE4_PIN     3
#define RELE5_PIN     4

// Vezerlo kimenetek 
#define IRANY_PIN     8

// Vezerlo bemenetek 
#define INDEX_J_BEMENET  5
#define INDEX_B_BEMENET  6
#define SEBVALTO_BEMENET 7
