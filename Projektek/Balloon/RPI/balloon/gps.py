def Gps_Init(gsm):
    print('Initializing GPS modem')

def Gps_Get_Data(gsm):
    print('Getting GPS data')

    # GPS adatok lekérése
    gps_data = gsm.GetActualGPS()
    # dummy string
    #             lat    lon  alt  speed
    # gps_data = '46.123 27.123 1234 123'
    return gps_data
