import os
from gsmHat import GSMHat, SMS, GPS

def Radio_Init():
    os.system('sudo amixer cset numid=3 1  # 3.5mm jack kimenet ')
    os.system('sudo raspi-gpio set 40 ip  # pin40 input ')
    os.system('sudo raspi-gpio set 45 ip  # pin45 input ')
    os.system('sudo raspi-gpio set 12 a0  # pin12 PWM0 (Alt0) ')
    os.system('sudo raspi-gpio set 13 a0  # pin13 PWM1 (Alt0) ')
    os.system('sudo raspi-gpio set 6 op  # PTT output ')
    os.system('sudo raspi-gpio set 6 dl  # PTT L ')
    os.system('sudo raspi-gpio set 40 ip  # pin40 input ')
    os.system('sudo raspi-gpio set 45 ip  # pin45 input ')
    os.system('sudo raspi-gpio set 12 a0  # pin12 PWM0 (Alt0) ')
    os.system('sudo raspi-gpio set 13 a0  # pin12 PWM0 (Alt0) ')
    os.system('sudo raspi-gpio set 6 op ')
    print ('Radio Init OK')


def Radio_play_file(filename):
    print('Playing file over radio')
    os.system('sudo raspi-gpio set 6 dh ')
    os.system('sleep 0.5 ')
    os.system('espeak -f ' + filename + ' --stdout | aplay ')
    os.system('sudo raspi-gpio set 6 dl ')

def Radio_send_report(GPS_data):
    print('Sending position over radio')

    # TODO: write to report.txt
    f = open("report.txt", "w")
    f.write('hotel alfa eight lima oscar uniform dash eleven .  \n')

    Message = 'Latitude: %s .   ' % str(GPS_data.Latitude) + 'Longitude: %s .   ' % str(
        GPS_data.Longitude) + 'Altitude: %s .  ' % str(GPS_data.Altitude) + 'Speed: %s .  ' % str(
        GPS_data.Speed) + 'Course: %s .  ' % str(
        GPS_data.Course)

    f.write(Message)
    f.close()

    Radio_play_file('report.txt')
