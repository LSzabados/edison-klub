#!/usr/bin/python
# Filename: balloon.py
import serial
import time
import os
from gsmHat import GSMHat, SMS, GPS

from radio import *
from radio2 import *
from gps import *
from gsm import *
from logger import *

print ('\n---------------------------------')
print ('Edison Klub Csorvas, Hungary 2021')
print ('---------------------------------\n')

# GSM is a global resource
gsm = GSMHat('/dev/ttyS0', 115200)
package_num = 0

Radio_Init()
Gsm_Init(gsm)
Gps_Init(gsm)

# Radio_play_file2('welcome.txt')

while True:

    gps_data = Gps_Get_Data(gsm)
    Logger_add(gps_data)    

    if 1 <= round(gps_data.Altitude) <= 3000:
        radio_scale = 10
        sleep_time = 10
    else:
        radio_scale = 5
        sleep_time = 30
        
    if 1 <= round(gps_data.Altitude) <= 5000:
        gsm_scale = 4 # every 40 seconds
    else:
        gsm_scale = 0 # flag number to switch off the sending above 3000 m

    if package_num % radio_scale == 0:
        Radio_send_report_LONG(gps_data)
    else:
        Radio_send_report_SHORT(gps_data)

    if gsm_scale != 0: # flag cheking
        if package_num % gsm_scale == 0:
            Gsm_Send_SMS(gsm, gps_data, package_num)
      
        # calculate sleep time from altitude
    print('Sleeping...\n '  )
    time.sleep(sleep_time)
    package_num += 1

# End of while loop
