from gsmHat import GSMHat, SMS, GPS



def Logger_add(GPS_data):

    Message = 'Ballon: %s ' % str(GPS_data.GNSS_status) + 'Fix_status: %s ' % str(GPS_data.Fix_status) + 'UTC: %s  ' % str(
        GPS_data.UTC) + 'Coord: %s , ' % str(GPS_data.Latitude) + '%s  ' % str(
        GPS_data.Longitude) + 'Altitude: %s ' % str(GPS_data.Altitude) + 'Speed: %s  ' % str(
        GPS_data.Speed) + 'Course: %s ' % str(GPS_data.Course)

    f = open("log.txt", "a")
    f.write('\n' + Message)
    f.flush()
    f.close()
