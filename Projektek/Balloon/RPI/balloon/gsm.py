from gsmHat import GSMHat, SMS, GPS


def Gsm_Init(gsm):
    print('Initializing GSM modem')


def Gsm_Send_SMS(gsm, GPS_data, gsm_ctr):

    # TODO: send data to modem via ser
    # SMS írás a megadott számra
    # Number = '+36206131994'
    Number = '+36702742704'
    Message = 'Msg: ' + str(gsm_ctr) + '\nFix: %s\n' % str(GPS_data.Fix_status) + 'UTC: %s\n' % str(
        GPS_data.UTC) + 'Coord: %s , ' % str(GPS_data.Latitude) + '%s\n' % str(
        GPS_data.Longitude) + 'Mag: %s\n' % str(round(GPS_data.Altitude)) + 'km/h: %s\n' % str(round(
        GPS_data.Speed)) + 'Irany: %s\n' % str(round(GPS_data.Course))

    # SMS küldése
    gsm.SMS_write(Number, Message)
    print('Sending SMS to : '+Number + "  " + Message  )

