import os
from gsmHat import GSMHat, SMS, GPS


def Radio_Init():
    os.system('sudo amixer cset numid=3 1  # 3.5mm jack kimenet ')
    os.system('sudo raspi-gpio set 40 ip  # pin40 input ')
    os.system('sudo raspi-gpio set 45 ip  # pin45 input ')
    os.system('sudo raspi-gpio set 12 a0  # pin12 PWM0 (Alt0) ')
    os.system('sudo raspi-gpio set 13 a0  # pin13 PWM1 (Alt0) ')
    os.system('sudo raspi-gpio set 6 op  # PTT output ')
    os.system('sudo raspi-gpio set 6 dl  # PTT L ')
    os.system('sudo raspi-gpio set 40 ip  # pin40 input ')
    os.system('sudo raspi-gpio set 45 ip  # pin45 input ')
    os.system('sudo raspi-gpio set 12 a0  # pin12 PWM0 (Alt0) ')
    os.system('sudo raspi-gpio set 13 a0  # pin12 PWM0 (Alt0) ')
    os.system('sudo raspi-gpio set 6 op ')
    print('Radio Init OK')


def Radio_play_file(filename):
    print('Playing file over radio')
    os.system('sudo raspi-gpio set 6 dh ')
    os.system('sleep 0.5 ')
    os.system('espeak -f ' + filename + ' --stdout | aplay ')
    os.system('sudo raspi-gpio set 6 dl ')


def Radio_play_file2(filename):
    print('Playing file over radio - HUNgarian')

    os.system('sudo raspi-gpio set 6 dh ')
    os.system('sleep 0.5 ')
    os.system('espeak -f ' + filename + ' -s 300 -v mb-hu1 --stdout | aplay ')
    # os.system("aplay koordinatak.mp3")
    os.system('sudo raspi-gpio set 6 dl ')


def Radio_send_report_LONG(GPS_data):
    print('Sending position over radio')

    # write to report.txt
    f = open("report.txt", "w")
    f.write('há a 8 el o u 11 .  \n')

    Message = 'Magasság: %s .  ' % str(int(GPS_data.Altitude)) \
        + 'Szélesség: %.5f .  ' % GPS_data.Latitude \
        + 'Hosszúság: %.5f .  ' % GPS_data.Longitude \
        + 'Sebesség: %s .  ' % str(int(GPS_data.Speed)) \
        + 'Kurzus: %s .  ' % str(int(GPS_data.Course)) 

#Message = 'Magasság: %s .  ' % str(int(GPS_data.Altitude)) \
#        + 'Szélesség: %.5f .  ' % (GPS_data.Latitude - int(GPS_data.Latitude)) \
#        + 'Hosszúság: %.5f .  ' % (GPS_data.Longitude - int(GPS_data.Longitude))

    f.write(Message)
    f.close()

    Radio_play_file2("report.txt")


def Radio_send_report_SHORT(GPS_data):
    print('Sending position over radio')

    # write to report.txt
    f = open("report_SHORT.txt", "w")
    # f.write('há a 8 el o u 11 .  \n')

#    Message2 = 'Magasság: %s .  ' % str(int(GPS_data.Altitude))  \
#    + 'Szélesség: %.5f .  ' % (GPS_data.Latitude - int(GPS_data.Latitude)) \
#    + 'Hosszúság: %.5f .  ' % (GPS_data.Longitude - int(GPS_data.Longitude))

    Message2 = 'Magasság: %s .  ' % str(int(GPS_data.Altitude))  \
     + 'Szélesség: %d .  ' % int(1e5*(GPS_data.Latitude - int(GPS_data.Latitude))) \
     + 'Hosszúság: %d .  ' % int(1e5*(GPS_data.Longitude - int(GPS_data.Longitude)))

    f.write(Message2)
    f.close()

    Radio_play_file2("report_SHORT.txt")
