#!/usr/bin/python
# Filename: loop.py

from gsmHat import GSMHat, SMS, GPS
import time

gsm = GSMHat('/dev/ttyS0', 115200)

base_altitude = 85.0
max_altitude = base_altitude

def send_VHF_report(GPS_data):
    f = open("report.txt", "a")
    f.write(GPS_data)
    f.close()

# Végtelen ciklus
while True:

    # GPS adatok lekérése
    GPS_data = gsm.GetActualGPS()

    # SMS írás a megadott számra
    Number = '+36206131994'
    Message = 'GNSS_status: %s' % str(GPS_data.GNSS_status) + 'Fix_status: %s' % str(GPS_data.Fix_status) + 'UTC: %s' % str(
        GPS_data.UTC) + 'Latitude: %s' % str(GPS_data.Latitude) + 'Longitude: %s' % str(
        GPS_data.Longitude) + 'Altitude: %s' % str(GPS_data.Altitude) + 'Speed: %s' % str(
        GPS_data.Speed) + 'Course: %s' % str(
        GPS_data.Course)

    # SMS küldése
    gsm.SMS_write(Number, Message)

    # report.txt-be írás
    send_VHF_report(Message)

    # emelkedés/süllyedés meghatározása, az eddig elért legmagasabb 'pont' függvényében
    current_altitude = GPS_data.Altitude

    if current_altitude > max_altitude:
        max_altitude = current_altitude
        climb = True
    else:
        climb = False

    if climb:
        if current_altitude <= 3000.0:
            time_by_altitude = 30
        else:
            time_by_altitude = 60
    else:
        if current_altitude <= 5000.0:
            time_by_altitude = 5
        else:
            time_by_altitude = 30

    time.sleep(time_by_altitude)
# Végtelen ciklus vége
